<?php

class Autoloader {
	protected $dirsArr;
	public function __construct() {
		$this->dirsArr = [ROOT_DIR . '/components/', ROOT_DIR . '/models/'];
		set_include_path(implode(PATH_SEPARATOR, $this->dirsArr));

		spl_autoload_register(function ($class) {
			include($class . '.php');
		});
	}
}
