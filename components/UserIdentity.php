<?php
class UserIdentity {
	public static function isUserGuest() {
		if (isset($_SESSION['name'])) {
            return false;
        }

        return true;
	}
	
	public static function auth($name) {

        $_SESSION['name'] = $name;
        $_SESSION['sort_field'] = 0;
        $_SESSION['sort_organize'] = 1;
    }

    public static function checkAuth() {

        if (isset($_SESSION['name'])) {
            return true;
        }

        header("Location: /user/login");
        return false;
    }
}