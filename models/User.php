<?php

class User {

    const ADMIN_LOGIN = "admin";
    const ADMIN_PASSWORD = "123";

    public static function checkUserData($name, $password) {

        if ($name == self::ADMIN_LOGIN && $password == self::ADMIN_PASSWORD) {
            return true;
        }

        return false;
    }

    public static function checkName($name) {

        if (strlen($name) >= 3 && strlen($name) <= 16) {
            return true;
        }

        return false;
    }

	public static function isGuest() {
		return UserIdentity::isUserGuest();
	}
}
