<?php include ROOT_DIR . '/views/layouts/header.php'; ?>

<div class="container theme-showcase" role="main">   
    <div class="page-header"><h1>Задача <?php echo $taskItem['task_id'] ?></h1></div>
    <p>
        <a href='/task/add' class="btn btn-primary" role="button">Добавить задачу</a>
		<?php if(!User::isGuest()) : ?>
        <a href='/task/edit/<?php echo $taskItem["task_id"] ?>' class="btn btn-warning" role="button">Редактировать</a>
		<?php endif; ?>
    </p>
    <p>
        <b>Пользователь: </b><?php echo $taskItem['task_user_name'] ?>
    </p>
    <p>
        <b>E-mail: </b><?php echo $taskItem['task_email'] ?>
    </p>
	<p>
        <b>Текст: </b><?php echo $taskItem['task_text'] ?>
    </p>
    <p>
        <b>Статус: </b>
		<?= $taskItem['task_status'] ? "Завершена" : "Новая"; ?>
    </p>  
</div> 

<?php include ROOT_DIR . '/views/layouts/footer.php'; ?>