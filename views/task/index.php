<?php include ROOT_DIR . '/views/layouts/header.php'; ?>

<div class="container theme-showcase" role="main">   
    <div class="page-header"><h1>Список задач</h1></div>        
    <p><a href='/task/add' class="btn btn-primary" role="button">Добавить задачу</a></p>
    <form class="form-inline" action="" method="post">
        <div class="form-group">
            <label>Сортировка:</label>
            <select class="form-control" name="sortField">
                <option value="0" <?php if ($_SESSION['sort_field'] == 0) echo 'selected="selected"'; ?>>Имя</option>
                <option value="1" <?php if ($_SESSION['sort_field'] == 1) echo 'selected="selected"'; ?>>E-mail</option>
                <option value="2" <?php if ($_SESSION['sort_field'] == 2) echo 'selected="selected"'; ?>>Статус</option>
            </select>
        </div>
        <div class="form-group">
            <label>Тип:</label>
            <select class="form-control" name="sortOrganize">
                <option value="1" <?php if ($_SESSION['sort_organize'] == 1) echo 'selected="selected"'; ?>>По возрастанию</option>
                <option value="2" <?php if ($_SESSION['sort_organize'] == 2) echo 'selected="selected"'; ?>>По убыванию</option>
            </select> 
            <input class="btn btn-default" type="submit" name="submit" value="Сортировать">  
        </div>
    </form>        
    <table class="table table-striped">
        <thead>       
            <tr>
                <td>Id</td>
                <td>Имя пользователя</td>
                <td>E-mail</td>
                <td>Текст</td>
                <td>Статус</td>
                <td></td>
                <?php if (!User::isGuest()) : ?>
                    <td></td>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($taskList)) : ?>
				<?php foreach ($taskList as $taskItem) : ?>
					<tr class="<?php if ($taskItem['task_status'] == 1) echo 'success'; ?>">
						<td><?php echo $taskItem['task_id'] ?></td>
						<td><?php echo $taskItem['task_user_name'] ?></td>
						<td><?php echo $taskItem['task_email'] ?></td>
						<td><?php echo $taskItem['task_text'] ?></td>
						<td>
							<?php
							if ($taskItem['task_status'] == 1) {
								echo "Выполнено";
							}
							else if($taskItem['task_status'] == 0) {
								echo "Новая";
							} else if($taskItem['task_status'] == 2){
								echo 'Отредактировано администратором<br />';
								echo "Выполнено";
							} else {
								echo 'Отредактировано администратором<br />';
								echo "Новая";
							}
							?>
						</td>
						<td>
							<a href="/task/<?php echo $taskItem['task_id'] ?>" class="btn btn-info" role="button">
								<span class="glyphicon glyphicon-eye-open"></span>
							</a>
						</td>
						<?php if (!User::isGuest()) : ?>
							<td>
								<a href="/task/edit/<?php echo $taskItem['task_id'] ?>" class="btn btn-warning" role="button">
									<span class="glyphicon glyphicon-pencil"></span>
								</a>
							</td>
						<?php endif; ?>
					</tr>
				<?php endforeach; ?> 
				<?php else: ?>
				<p>Задачи отсутствуют</p>
			<?php endif; ?>
        </tbody>
    </table>
    <p><?php echo $pagination->build(); ?></p>
</div>   

<?php include ROOT_DIR . '/views/layouts/footer.php'; ?>