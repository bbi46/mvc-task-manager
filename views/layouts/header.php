<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="favicon.ico">

        <title>Приложение-задачник</title>

        <link href="../../web/css/bootstrap.min.css" rel="stylesheet">
        <link href="../../web/css/bootstrap-theme.min.css" rel="stylesheet">
    </head>   
    <body>  
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="/">Приложение-задачник</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="<?php if (!UrlManager::getURI()) echo "active"; ?>"><a href="/">Домой</a></li>
						<li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <?php if (User::isGuest()) : ?>
                                    Привет, <b>Гость!</b>
                                <?php else : ?>
                                    Привет, <b><?php echo $_SESSION['name'] ?></b>
                                <?php endif; ?>    
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><?php if (User::isGuest()) : ?>
                                        <a href='/user/login'>Вход</a>
                                    <?php else : ?>
                                        <a href='/user/logout'>Выход</a>
                                    <?php endif; ?>       
                                </li>
                            </ul>
                        </li>
					</ul>
				</div>
			</div>
        </nav>
        
        <br>