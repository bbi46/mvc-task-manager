<?php

session_start();
ini_set('display_errors', 1);
define('ROOT_DIR', dirname(__FILE__) . '/..');
require_once(ROOT_DIR . '/components/Autoloader.php');

$autoloader = new Autoloader;
$urlManager = new UrlManager;
$urlManager->run();

