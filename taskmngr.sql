-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Мар 24 2021 г., 10:48
-- Версия сервера: 5.6.39-83.1
-- Версия PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `taskmngr`
--

-- --------------------------------------------------------

--
-- Структура таблицы `task`
--

CREATE TABLE IF NOT EXISTS `task` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `task_user_name` varchar(255) NOT NULL,
  `task_email` varchar(255) NOT NULL,
  `task_text` text NOT NULL,
  `task_status` int(11) NOT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `task`
--

INSERT INTO `task` (`task_id`, `task_user_name`, `task_email`, `task_text`, `task_status`) VALUES
(1, 'User2103240337', 'User2103240337@gmail.com', 'Закончить тестовое задание в BeeJee', 1),
(2, 'User2103240617', 'vank300828@gmail.com', 'Пройти собеседование в BeeJee', 0),
(3, 'User2103240616', 'vank300829@gmail.com', 'Устроиться в BeeJee на должность Junior Web-разработчик (PHP, JS)', 0),
(4, 'User2103240615', 'vank300827@gmail.com', 'Дорасти до уровня Middle Web-разработчик (PHP, JS)', 0),
(5, 'User2103240610', 'vank300826@gmail.com', 'Вырасти до Senior Web-разработчик (PHP, JS) через 3,5 года', 2),
(6, 'User2103240629', 'vank300824@gmail.com', 'Успешно сотрудничать с компанией BeeJee', 0),
(7, 'User2103240755', 'vank300831@gmail.com', 'Заработать на квартиру, сотрудничая с компанией BeeJee', 0),
(8, 'User2103240824', 'vank300832@gmail.com', '&lt;script&gt;alert(\'test\');&lt;/script&gt;', 0),
(9, 'test', 'test@test.com', 'test job 22', 3),
(10, 'test2', 'test2@test.com', 'test job 2', 0),
(11, 'test-admin', 'test-admin-job@test.com', 'test admin job', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
