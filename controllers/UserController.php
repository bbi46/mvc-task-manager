<?php

class UserController {

    public function actionLogin() {

        if (User::isGuest()) {
            if (isset($_POST['submit'])) {
                $name = htmlspecialchars($_POST['name']);
                $password = htmlspecialchars($_POST['password']);

                $errors = false;

                if (!User::checkName($name)) {
                    $errors[] = 'Имя должно быть от 3 до 16 символов!';
                }

                $userChecked = User::checkUserData($name, $password);

                if ($userChecked == false) {
                    $errors[] = 'Неправильный логин или пароль!';
                } else {
                    UserIdentity::auth($name);
                    header("Location: /");
                }
            }
        } else {
            header("Location: /");
        }

        require_once ROOT_DIR . '/views/user/login.php';
        return true;
    }

    public function actionLogout() {

        unset($_SESSION['name']);
		$_SESSION['sort_field'] = 0;
		$_SESSION['sort_organize'] = 1;
        header("Location: /");
    }
}
